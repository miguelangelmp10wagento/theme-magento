<?php

namespace Wagento\ModuleOrdersPendingToCancel\Cron;

use Wagento\ModuleOrdersPendingToCancel\Model\OrdersPendingToCancel;
use Wagento\ModuleOrdersPendingToCancel\Logger\Logger;

class OrdersPendingToCancelCron
{

    protected OrdersPendingToCancel $ordersPendingToCancel;
    protected Logger $logger;

    public function __construct(OrdersPendingToCancel $ordersPendingToCancel, Logger $logger)
    {
        $this->ordersPendingToCancel = $ordersPendingToCancel;
        $this->logger = $logger;
    }

    /**
     *
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
        try {
            $this->ordersPendingToCancel->execute();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
