<?php

namespace Wagento\ModuleOrdersPendingToCancel\Model;

use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Sales\Model\ResourceModel\Order as ResourceModelOrder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Wagento\ModuleOrdersPendingToCancel\Logger\Logger;

class OrdersPendingToCancel
{

    protected Logger $logger;
    protected OrderRepository $orderRepo;
    protected SearchCriteriaBuilder $searchCriteriaBuilder;
    protected SortOrderBuilder $sortOrderBuilder;
    protected FilterBuilder $filterBuilder;
    protected ScopeConfigInterface $scopeConfig;
    protected ResourceModelOrder $orderResourceModel;

    public function __construct(
        Logger                $logger,
        OrderRepository       $orderRepo,
        ResourceModelOrder    $orderResourceModel,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder      $sortOrderBuilder,
        FilterBuilder         $filterBuilder,
        ScopeConfigInterface  $scopeConfigInterface
    ) {
        $this->orderRepo = $orderRepo;
        $this->logger = $logger;
        $this->orderResourceModel = $orderResourceModel;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->scopeConfig = $scopeConfigInterface;
    }

    /**
     *
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
        $orderData = $this->orderRepo->getList($this->getSearchCriteria());

        if ($orderData->getTotalCount() > 0) {
            $orders = $orderData->getItems();
            $this->processOrders($orders);
        } else {
            $this->logger->info("There are no orders to process.");
        }
    }

    private function getSearchCriteria(): SearchCriteria
    {
        $pathNumberDays = 'sales/orders_pending_to_cancel/number_days_order_cancellation';
        $pathNumberOrder = 'sales/orders_pending_to_cancel/number_order_cancellation';
        $numberDaysOrder = $this->scopeConfig
            ->getValue($pathNumberDays, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $numberOrderCancellation = $this->scopeConfig
            ->getValue($pathNumberOrder, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $to = date("Y-m-d h:i:s");
        $from = strtotime("-$numberDaysOrder days", strtotime($to));
        $from = date('Y-m-d h:i:s', $from);

        return $this->searchCriteriaBuilder
            ->addFilter("status", Order::STATE_PROCESSING)
            ->addFilter('created_at', $from, 'gteq')
            ->addFilter('created_at', $to, 'lteq')
            ->setPageSize($numberOrderCancellation)
            ->create();
    }

    /**
     * @throws AlreadyExistsException
     */
    private function processOrders($orders)
    {
        foreach ($orders as $order) {
            $state = Order::STATE_CANCELED;
            $order->setStatus($state)->setState($state);
            try {
                $this->orderResourceModel->save($order);
                $this->logger->info("Update order ID " . $order->getIncrementId() . " " . $order->getCustomerEmail());
            } catch (AlreadyExistsException|\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
    }
}
