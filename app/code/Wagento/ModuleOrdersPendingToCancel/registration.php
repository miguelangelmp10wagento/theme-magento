<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Wagento_ModuleOrdersPendingToCancel', __DIR__);
