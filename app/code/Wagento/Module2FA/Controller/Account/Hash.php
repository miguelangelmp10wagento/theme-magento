<?php

namespace Wagento\Module2FA\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Wagento\Module2FA\Model\Hash\CustomerSession;

class Hash extends Action
{
    private $customerSession;
    private $accountRedirect;

    public function __construct(
        Context                     $context,
        CustomerSession $customerSession
    ) {
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $hash = $this->getRequest()->getParam('hash');
        try {
            return $this->customerSession->validateHash($hash);
        } catch (NoSuchEntityException | LocalizedException $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
            return $this->accountRedirect->getRedirect();
        }
    }
}
