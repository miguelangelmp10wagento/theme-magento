<?php

namespace Wagento\Module2FA\Controller\Account;

use Magento\Framework\App\Action\Context;
use Wagento\Module2FA\Model\Login\CustomerHash;

class Loginhashpost extends \Magento\Framework\App\Action\Action
{
    private CustomerHash $customerHash;

    public function __construct(
        Context      $context,
        CustomerHash $customerHash
    ) {
        $this->customerHash = $customerHash;
        parent::__construct($context);
    }

    public function execute()
    {
        $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            $this->customerHash->postSendEmail($login['username']);
        }
        return $redirect->setUrl('/customer/account/loginbyhash');
    }
}
