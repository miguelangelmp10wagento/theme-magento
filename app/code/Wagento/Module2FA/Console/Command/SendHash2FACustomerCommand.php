<?php

namespace Wagento\Module2FA\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;
use Wagento\Module2FA\Model\Login\CustomerHash;

class SendHash2FACustomerCommand extends Command
{
    private const EMAIL = 'email';

    private CustomerHash $customerHash;
    private State $state;

    public function __construct(CustomerHash $customerHash, State $state)
    {
        $this->state = $state;
        $this->customerHash = $customerHash;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('customer:send:hash');
        $this->setDescription('This command will send an email with a link to log in to the store.');
        $this->addOption(
            self::EMAIL,
            '-e',
            InputOption::VALUE_REQUIRED,
            'Email'
        );
        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($email = $input->getOption(self::EMAIL)) {
            try {
                $this->state->setAreaCode(Area::AREA_FRONTEND);
                $this->customerHash->updateHashExpirationHash($email);
                $output->writeln('<info>Email access was sent: ' . $email . '</info>');
            } catch (NoSuchEntityException | LocalizedException $e) {
                $output->writeln('<error>' . $e->getLogMessage() . '</error>');
            }
        } else {
            $output->writeln('<error>Email is required and cannot be empty</error>');
        }
    }
}
