<?php

namespace Wagento\Module2FA\Model\Login;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\ScopeInterface;

class CustomerHash
{
    private PageFactory $resultPageFactory;
    protected TransportBuilder $transportBuilder;
    protected $customerCollectionFactory;
    protected CustomerRepositoryInterface $customerRepository;
    private StoreManagerInterface $storeManager;
    private EncryptorInterface $encryptor;
    private ScopeConfigInterface $scopeConfig;
    private CustomerFactory $customerFactory;
    private ManagerInterface $messageManager;

    public function __construct(
        PageFactory                 $resultPageFactory,
        TransportBuilder            $transportBuilder,
        StoreManagerInterface       $storeManager,
        CustomerFactory             $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface        $scopeConfigInterface,
        EncryptorInterface          $encryptorInterface,
        ManagerInterface            $messageManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->scopeConfig = $scopeConfigInterface;
        $this->encryptor = $encryptorInterface;
        $this->messageManager = $messageManager;
    }

    public function postSendEmail($email)
    {
        if (!empty($email)) {
            try {
                $email = $this->customerFactory->create()
                    ->getCollection()
                    ->addFilter("email", $email)
                    ->getFirstItem()->getData("email");
                // phpcs:ignore
                if (!is_null($email)) {

                    $this->updateHashExpirationHash($email);

                    $this->messageManager->addSuccessMessage(__("Email access was sent: $email."));
                } else {
                    $this->messageManager->addErrorMessage(__('There are no records.'));
                }
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
            }
        } else {
            $this->messageManager->addErrorMessage(__('The Email is required.'));
        }
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function sendEmail($email, $hash)
    {
        $transport = $this->transportBuilder
            ->setTemplateIdentifier('email_custom_template')
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars([
                'hash' => $hash,
                'link' => $this->storeManager->getStore()->getBaseUrl() . "customer/account/hash/hash/"

            ])
            ->addTo([$email])
            ->setFrom('general')
            ->getTransport();

        $transport->sendMessage();
    }

    private function getExpirationHash()
    {
        $pathExpirationHash = 'customer/startup/expiration_hash';
        $expirationHash = $this->scopeConfig->getValue($pathExpirationHash, ScopeInterface::SCOPE_STORE);
        $format = 'Y-m-d H:i:s';
        $date = date($format);
        $newDate = strtotime("+$expirationHash hour", strtotime($date));
        return date($format, $newDate);
    }

    /**
     * @param $email
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateHashExpirationHash($email): void
    {
        $hash = $this->encryptor->getHash(microtime(), false);

        $customer = $this->customerFactory
            ->create()
            ->setWebsiteId($this->storeManager->getStore()->getId())
            ->loadByEmail($email);

        $customer->setHash($hash);
        $customer->setExpirationHash($this->getExpirationHash());

        $customer->save();

        $this->sendEmail($email, $hash);
    }
}
