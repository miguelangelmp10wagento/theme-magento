<?php

namespace Wagento\Module2FA\Model\Hash;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\Data\Form\FormKey;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\Exception\NoSuchEntityException;

class CustomerSession
{

    private $customerSession;

    /**
     * @var AccountManagementInterface
     */
    private $customerAccountManagement;

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private $cookieMetadataManager;

    private $customerCollectionFactory;
    private $messageManager;
    private FormKey $formKey;

    public function __construct(
        Session                     $session,
        FormKey                     $formKey,
        AccountManagementInterface  $customerAccountManagement,
        CustomerFactory             $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        AccountRedirect             $accountRedirect,
        Collection                  $customerCollectionFactory
    ) {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->customerSession = $session;
        $this->formKey = $formKey;
        $this->accountRedirect = $accountRedirect;
        $this->customerCollectionFactory = $customerCollectionFactory;
    }

    /**
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateHash($hash)
    {
        $customerHash = $this->customerCollectionFactory
            ->addAttributeToSelect("*")
            ->addAttributeToFilter('hash', $hash)
            ->getFirstItem();
        //phpcs:ignore
        if (!is_null($customerHash->getEmail())) {
            $customer = $this->customerRepository->get($customerHash->getEmail());
            if (!isset($customer)) {
                throw new NoSuchEntityException();
            } else {
                if (strtotime(date('Y-m-d H:i:s', time())) <= strtotime($customerHash->getExpirationHash())) {
                    $this->registerCustomerSession($customer);
                } else {
                    $this->messageManager->addErrorMessage(__("Expired hash."));
                }
                return $this->accountRedirect->getRedirect();
            }
        } else {
            $this->messageManager->addErrorMessage(__("Incorrect hash."));
            return $this->accountRedirect->getRedirect();
        }
    }

    /**
     * Register customer session
     *
     */
    private function registerCustomerSession($customer)
    {
        $this->customerSession->setCustomerDataAsLoggedIn($customer);

        if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
            $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
            $metadata->setPath('/');
            $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
        }
    }

    /**
     * Retrieve cookie manager
     *
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     * @deprecated 100.1.0
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     * @deprecated 100.1.0
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    public function _whitelistEndpoint()
    {
        // CSRF Magento2.3 compatibility
        //phpcs:ignore
        if (interface_exists("\Magento\Framework\App\CsrfAwareActionInterface")) {
            // @phpstan-ignore-next-line
            $request = $this->getRequest();
            if ($request instanceof \HttpRequest && $request->isPost() && empty($request->getParam('form_key'))) {
                $request->setParam('form_key', $this->formKey->getFormKey());
            }
        }
    }
}
