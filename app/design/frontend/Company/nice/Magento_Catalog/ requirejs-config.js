var config = {
    paths: {
        'customjs': "Magento_Catalog/js/customjs"
    },
    shim: {
        'customjs': {
            deps: ['jquery']
        }
    },
    map: {
        '*': {
            custom_js: "Magento_Catalog/js/customjs"
        }
    },
};
